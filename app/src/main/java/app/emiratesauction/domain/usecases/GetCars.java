package app.emiratesauction.domain.usecases;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import app.emiratesauction.data.api.callbacks.BaseCallbackWithList;
import app.emiratesauction.data.api.repository.CarsRepository;
import app.emiratesauction.data.models.Car;
import app.emiratesauction.domain.base.UseCase;

public class GetCars extends UseCase<GetCars.RequestValues, GetCars.ResponseValue> {
    private CarsRepository carsRepository;

    public GetCars(CarsRepository carsRepository) {
        this.carsRepository = carsRepository;
    }

    @Override
    protected void executeUseCase(RequestValues requestValues) {
        carsRepository.getCars(new BaseCallbackWithList<Car>() {
            @Override
            public void success(List<Car> ListOfData) {
                // get current date-time parse it to "dd MMM hh:mm a" and add 5 mins
                // for the sake of demo, i've intercepted the second car and changed
                // the end data to be less than 5 mins so you can this feature can be tested easy.
                Calendar tmp = Calendar.getInstance();
                tmp.add(Calendar.MINUTE, 4);
                SimpleDateFormat sdf = new SimpleDateFormat("dd MMM hh:mm a", Locale.ENGLISH);

                ListOfData.get(1).getAuctionInfo().setEndDateEn(sdf.format(tmp.getTimeInMillis()));

                getUseCaseCallback().onSuccess(new ResponseValue(ListOfData));
            }

            @Override
            public void error() {
                getUseCaseCallback().onError();
            }
        });
    }

    public static final class RequestValues implements UseCase.RequestValues {

    }

    public static final class ResponseValue implements UseCase.ResponseValue {
        private List<Car> cars;

        public ResponseValue(List<Car> cars) {
            this.cars = cars;
        }

        public List<Car> getCars() {
            return cars;
        }
    }
}
