package app.emiratesauction.presentation.screens.carList;

import java.util.List;

import app.emiratesauction.data.models.Car;
import app.emiratesauction.presentation.base.BaseContract;

public interface CarListContract {
    interface View extends BaseContract.View {
        void initializeView();

        void initializeListAdapter();

        void initializeTimerTask();

        void showListErrorLoading();

        void hideListErrorLoading();

        void showSwipeToRefreshLoading();

        void hideSwipeToRefreshLoading();

        void addCars(List<Car> cars);

        void setCars(List<Car> cars);

        void resetList();

        void showLanguagePicker(int selectedLangIndex);

        void changeLanguage(String language);

        void restart();

        void showSortOptionsDialog(int selectedOption);
    }

    interface Presenter extends BaseContract.Presenter<BaseContract.View> {
        void start();

        void onRefresh();

        void onRetryFromErrorLoading();

        void onLanguageClicked(String currentLang);

        void onLanguagePicked(int selectedLangIndex, String currentLang);

        void onSortClicked();

        void onSortOptionPicked(int selectedOption);

        void onFavoriteClicked(int carId);
    }
}
