package app.emiratesauction.presentation.screens.carList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import app.emiratesauction.data.models.Car;
import app.emiratesauction.domain.base.UseCase;
import app.emiratesauction.domain.base.UseCaseHandler;
import app.emiratesauction.domain.usecases.GetCars;
import app.emiratesauction.presentation.base.BaseContract;
import app.emiratesauction.presentation.base.BasePresenter;

import static app.emiratesauction.presentation.utils.LocaleHelper.ARABIC_KEY;
import static app.emiratesauction.presentation.utils.LocaleHelper.ENGLISH_KEY;

public class CarListPresenter extends BasePresenter implements CarListContract.Presenter {
    private static final int ARABIC_INDEX = 0;
    private static final int ENGLISH_INDEX = 1;

    private CarListContract.View mView;

    private UseCaseHandler useCaseHandler;
    private GetCars getItems;

    private List<Car> carList = new ArrayList<>();

    private int selectedSortOption = 0;

    public CarListPresenter(UseCaseHandler useCaseHandler, GetCars getItems) {
        this.useCaseHandler = useCaseHandler;
        this.getItems = getItems;
    }

    @Override
    public void registerView(BaseContract.View view) {
        super.registerView(view);
        mView = (CarListContract.View) view;
    }

    @Override
    public void start() {
        mView.initializeView();
        mView.initializeListAdapter();
        mView.initializeTimerTask();

        getCars(false);
    }

    @Override
    public void onRefresh() {
        selectedSortOption = 0;
        getCars(true);
    }

    @Override
    public void onRetryFromErrorLoading() {
        mView.hideListErrorLoading();
        getCars(false);
    }

    @Override
    public void onLanguageClicked(String currentLang) {
        mView.showLanguagePicker(currentLang.equals(ARABIC_KEY) ? ARABIC_INDEX : ENGLISH_INDEX);
    }

    @Override
    public void onLanguagePicked(int selectedLangIndex, String currentLang) {
        // 0. check if it's a different language
        int currentLangIndex = currentLang.equals(ARABIC_KEY) ? ARABIC_INDEX : ENGLISH_INDEX;

        if (selectedLangIndex == currentLangIndex) return;

        mView.changeLanguage(selectedLangIndex == ARABIC_INDEX ? ARABIC_KEY : ENGLISH_KEY);

        mView.restart();
    }

    @Override
    public void onSortClicked() {
        mView.showSortOptionsDialog(selectedSortOption);
    }

    @Override
    public void onSortOptionPicked(final int selectedOption) {
        if (selectedOption == selectedSortOption) return;

        selectedSortOption = selectedOption;

        // sorting
        Collections.sort(carList, new Comparator<Car>() {
            public int compare(Car obj1, Car obj2) {

                switch (selectedOption) {
                    case 0:
                        return obj2.getAuctionInfo().getBids().compareTo(obj1.getAuctionInfo().getBids());
                    case 1:
                        return obj2.getAuctionInfo().getCurrentPrice().compareTo(obj1.getAuctionInfo().getCurrentPrice());
                    case 2:
                        return obj2.getAuctionInfo().getEndDate().compareTo(obj1.getAuctionInfo().getEndDate());
                    case 3:
                        return obj2.getYear().compareTo(obj1.getYear());
                    default:
                        return obj2.getAuctionInfo().getBids().compareTo(obj1.getAuctionInfo().getBids());
                }
            }
        });

        mView.initializeListAdapter();
        mView.setCars(carList);
    }

    @Override
    public void onFavoriteClicked(int carId) {
        // update this model and set carList again
        Car car = carList.get(getCarIndexById(carId));
        car.setFavorite(!car.isFavorite());

        mView.setCars(carList);
    }

    private void getCars(final boolean swipeToRefresh) {
        if (swipeToRefresh) mView.showSwipeToRefreshLoading();
        else mView.showProgress();

        useCaseHandler.execute(getItems, new GetCars.RequestValues(),
                new UseCase.UseCaseCallback<GetCars.ResponseValue>() {
                    @Override
                    public void onSuccess(GetCars.ResponseValue response) {
                        if (swipeToRefresh) mView.hideSwipeToRefreshLoading();
                        else mView.hideProgress();

                        if (swipeToRefresh) {
                            carList.clear();
                            mView.resetList();
                            mView.initializeListAdapter();
                        }

                        addAllCars(response.getCars());

                        mView.addCars(carList);
                    }

                    @Override
                    public void onError() {
                        mView.hideProgress();
                        mView.hideSwipeToRefreshLoading();
                        mView.showListErrorLoading();
                    }
                });
    }

    private void addAllCars(List<Car> cars) {
        for (Car car : cars)
            if (!carList.contains(car))
                carList.add(car);
    }

    private Car getCarById(int carId) {
        for (Car car : carList)
            if (car.getCarID() == carId)
                return car;
        return null;
    }

    private int getCarIndexById(int carId) {
        for (int i = 0; i < carList.size(); i++)
            if (carList.get(i).getCarID() == carId)
                return i;

        return -1;
    }
}
