package app.emiratesauction.presentation.screens;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import app.emiratesauction.data.models.Car;

public class CarViewModel {
    private Integer carID;
    private String image, name, price, currency, timeLeft;
    private long lotsNum, bidsNum;

    private long diffInSeconds;
    private boolean isFavorite;

    public CarViewModel(Car car, boolean isArabicLocale) {
        this.carID = car.getCarID();
        this.isFavorite = car.isFavorite();

        String carImage = car.getImage();
        carImage = carImage.replace("[w]", "500")
                .replace("[h]", "500");
        this.image = carImage;

        String carMake = isArabicLocale ? car.getMakeAr() : car.getMakeEn();
        String carModel = isArabicLocale ? car.getModelAr() : car.getModelEn();
        this.name = carMake + " " + carModel + " " + car.getYear();

        this.price = NumberFormat.getNumberInstance(Locale.US).format(car.getAuctionInfo().getCurrentPrice());

        this.currency = isArabicLocale ? car.getAuctionInfo().getCurrencyAr()
                : car.getAuctionInfo().getCurrencyEn();

        this.lotsNum = car.getAuctionInfo().getLot();
        this.bidsNum = car.getAuctionInfo().getBids();

        // the end date as a text has no year no we need to add it and 2018 for the sake of the demo
        // endDate is not parcelable as long, i don't know if it represents the endTime in seconds or in millis or what
        String endDate = "2018 " + car.getAuctionInfo().getEndDateEn();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy dd MMM hh:mm a", Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long lEndDate = date.getTime();
        long timeNow = Calendar.getInstance().getTimeInMillis();

        diffInSeconds = (lEndDate - timeNow) / 1000;

        long days = TimeUnit.SECONDS.toDays(diffInSeconds);
        diffInSeconds -= TimeUnit.DAYS.toDays(days) * 24 * 60 * 60;

        long hours = TimeUnit.SECONDS.toHours(diffInSeconds);
        diffInSeconds -= TimeUnit.HOURS.toHours(hours) * 60 * 60;

        long minutes = TimeUnit.SECONDS.toMinutes(diffInSeconds);

        this.timeLeft = String.format(Locale.ENGLISH, "%02d", days) + ":"
                + String.format(Locale.ENGLISH, "%02d", hours) + ":"
                + String.format(Locale.ENGLISH, "%02d", minutes);
    }

    public Integer getCarID() {
        return carID;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }

    public long getLotsNum() {
        return lotsNum;
    }

    public long getBidsNum() {
        return bidsNum;
    }

    public String getTimeLeft() {
        return timeLeft;
    }

    public long getDiffInSeconds() {
        return diffInSeconds;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
