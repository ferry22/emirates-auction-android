package app.emiratesauction.presentation.screens.carList;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.airbnb.epoxy.EpoxyRecyclerView;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.List;

import javax.inject.Inject;

import app.emiratesauction.EmiratesAuctionApp;
import app.emiratesauction.R;
import app.emiratesauction.data.models.Car;
import app.emiratesauction.presentation.adapters.CarsAdapter;
import app.emiratesauction.presentation.base.BaseContract;
import app.emiratesauction.presentation.base.BaseFragment;
import app.emiratesauction.presentation.utils.LocaleHelper;
import app.emiratesauction.presentation.views.cars.CarView;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class CarListFragment extends BaseFragment implements CarListContract.View, SwipeRefreshLayout.OnRefreshListener, View.OnClickListener, CarView.Listener {
    @Inject
    CarListContract.Presenter mPresenter;

    @BindView(R.id.cars_rv)
    EpoxyRecyclerView carsRV;
    @BindView(R.id.swipe_to_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindColor(R.color.colorAccent)
    int colorAccent;

    private CarsAdapter carsAdapter;
    private boolean isArabicLocale;

    public CarListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setHasOptionsMenu(true);
        initializeToolbar(true, getString(R.string.app_name), false);

        isArabicLocale = LocaleHelper.isArabicLocale(getActivity());

        mPresenter.start();
    }

    @Override
    public BaseContract.Presenter<BaseContract.View> getPresenter() {
        return mPresenter;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_carlist;
    }

    @Override
    protected void injectDependencies() {
        ((EmiratesAuctionApp) getActivity().getApplication()).getAppComponent().inject(this);
    }

    @Override
    public void showProgress() {
        carsAdapter.setLoading(true);
    }

    @Override
    public void hideProgress() {
        carsAdapter.setLoading(false);
    }

    @Override
    public void initializeView() {
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeColors(colorAccent);
    }

    @Override
    public void initializeListAdapter() {
        carsAdapter = new CarsAdapter();
        carsAdapter.setArabicLocale(isArabicLocale);
        carsAdapter.setErrorViewListener(this);
        carsAdapter.setCarViewListener(this);

        if (carsRV != null)
            carsRV.setController(carsAdapter);
    }

    @Override
    public void initializeTimerTask() {
        final Handler handler = new Handler();
        final int delay = 60 * 1000; // let the refresh interval be 1 minute for the sake of demo

        handler.postDelayed(new Runnable() {
            public void run() {
                mPresenter.onRefresh();
                handler.removeCallbacksAndMessages(null);

                handler.postDelayed(this, delay);
            }
        }, delay);
    }

    @Override
    public void showListErrorLoading() {
        carsAdapter.setErrorLoading(true);
    }

    @Override
    public void hideListErrorLoading() {
        carsAdapter.setErrorLoading(false);
    }

    @Override
    public void showSwipeToRefreshLoading() {
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideSwipeToRefreshLoading() {
        if (swipeRefreshLayout != null) swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void addCars(List<Car> cars) {
        carsAdapter.addCars(cars);
    }

    @Override
    public void setCars(List<Car> cars) {
        carsAdapter.setCars(cars);
    }

    @Override
    public void resetList() {
        carsAdapter.clear();
    }

    @Override
    public void showLanguagePicker(int selectedLangIndex) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.action_language)
                .items(R.array.languages)
                .itemsCallbackSingleChoice(selectedLangIndex, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        mPresenter.onLanguagePicked(which, LocaleHelper.getLanguage(getActivity()));
                        return true;
                    }
                })
                .show();
    }

    @Override
    public void changeLanguage(String language) {
        LocaleHelper.setLocale(getActivity(), language);
    }

    @Override
    public void restart() {
        getActivity().finish();
        getActivity().startActivity(getActivity().getIntent());
    }

    @Override
    public void showSortOptionsDialog(int selectedOption) {
        new MaterialDialog.Builder(getActivity())
                .title(R.string.title_sort)
                .items(R.array.sort_options)
                .itemsCallbackSingleChoice(selectedOption, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        mPresenter.onSortOptionPicked(which);
                        return true;
                    }
                })
                .show();
    }

    @Override
    public void onRefresh() {
        mPresenter.onRefresh();
    }

    @Override
    public void onClick(View v) {
        mPresenter.onRetryFromErrorLoading();
    }

    @OnClick(R.id.btn_sort)
    public void onSortClicked() {
        mPresenter.onSortClicked();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_language, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_langugae:
                mPresenter.onLanguageClicked(LocaleHelper.getLanguage(getActivity()));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFavoriteClicked(int carId) {
        mPresenter.onFavoriteClicked(carId);
    }
}