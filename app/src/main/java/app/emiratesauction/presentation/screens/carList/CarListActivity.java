package app.emiratesauction.presentation.screens.carList;

import android.support.v4.app.Fragment;

import app.emiratesauction.presentation.base.BaseFragmentActivity;

public class CarListActivity extends BaseFragmentActivity {
    @Override
    protected Fragment getFragment() {
        return new CarListFragmentBuilder().build();
    }
}
