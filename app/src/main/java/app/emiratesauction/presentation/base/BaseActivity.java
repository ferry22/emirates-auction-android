package app.emiratesauction.presentation.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import app.emiratesauction.EmiratesAuctionApp;
import app.emiratesauction.presentation.utils.LocaleHelper;

/**
 * All activities should inherit from this class
 * It will have all activities common functions
 */

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((EmiratesAuctionApp) getApplication()).getAppComponent().inject(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }
}
