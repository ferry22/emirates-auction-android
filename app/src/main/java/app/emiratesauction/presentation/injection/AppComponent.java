package app.emiratesauction.presentation.injection;

import javax.inject.Singleton;

import app.emiratesauction.presentation.base.BaseActivity;
import app.emiratesauction.presentation.base.BaseFragment;
import app.emiratesauction.presentation.screens.carList.CarListFragment;
import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {
    void inject(BaseActivity baseActivity);

    void inject(BaseFragment baseFragment);

    void inject(CarListFragment carListFragment);
}
