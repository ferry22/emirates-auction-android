package app.emiratesauction.presentation.injection;

import android.content.Context;

import app.emiratesauction.data.api.AuctionsApi;
import app.emiratesauction.data.api.repository.CarsRepository;
import app.emiratesauction.data.api.repository.local.CarsLocalDataSource;
import app.emiratesauction.data.api.repository.remote.CarsRemoteDataSource;
import app.emiratesauction.data.db.AppDatabase;
import app.emiratesauction.domain.base.UseCaseHandler;
import app.emiratesauction.domain.usecases.GetCars;
import app.emiratesauction.presentation.screens.carList.CarListContract;
import app.emiratesauction.presentation.screens.carList.CarListPresenter;
import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class AppModule {
    private Context mContext;

    public AppModule(Context context) {
        mContext = context;
    }

    @Provides
    Context provideContext() {
        return mContext;
    }

    @Provides
    AuctionsApi provideAuctionsApi(Retrofit retrofit) {
        return retrofit.create(AuctionsApi.class);
    }

    @Provides
    AppDatabase provideAppDatabase(Context context) {
        return AppDatabase.getAppDatabase(context);
    }

    @Provides
    CarsRemoteDataSource provideCarsRemoteDataSource(AuctionsApi auctionsApi) {
        return CarsRemoteDataSource.getInstance(auctionsApi);
    }

    @Provides
    CarsLocalDataSource provideCarsLocalDataSource(AppDatabase appDatabase) {
        return CarsLocalDataSource.getInstance(appDatabase);
    }

    @Provides
    CarsRepository provideCarsRepository(CarsRemoteDataSource carsRemoteDataSource,
                                         CarsLocalDataSource carsLocalDataSource) {
        return CarsRepository.getInstance(carsRemoteDataSource, carsLocalDataSource);
    }

    @Provides
    GetCars provideGetCars(CarsRepository carsRepository) {
        return new GetCars(carsRepository);
    }

    @Provides
    UseCaseHandler provideUseCaseHandler() {
        return UseCaseHandler.getInstance();
    }

    @Provides
    CarListContract.Presenter provideCarListPresenter(UseCaseHandler useCaseHandler,
                                                      GetCars getCars) {
        return new CarListPresenter(useCaseHandler, getCars);
    }
}
