package app.emiratesauction.presentation.adapters;

import android.view.View;

import com.airbnb.epoxy.AutoModel;
import com.airbnb.epoxy.EpoxyController;

import java.util.ArrayList;
import java.util.List;

import app.emiratesauction.data.models.Car;
import app.emiratesauction.presentation.screens.CarViewModel;
import app.emiratesauction.presentation.views.ErrorLoadingViewModel_;
import app.emiratesauction.presentation.views.cars.CarListLoadingViewModel_;
import app.emiratesauction.presentation.views.cars.CarView;
import app.emiratesauction.presentation.views.cars.CarViewModel_;

public class CarsAdapter extends EpoxyController {
    @AutoModel
    CarListLoadingViewModel_ loadingViewModel_;
    @AutoModel
    ErrorLoadingViewModel_ errorLoadingViewModel_;

    private boolean loading;
    private boolean errorLoading;

    private CarView.Listener carViewListener;
    private View.OnClickListener errorViewListener;

    private List<Car> carList = new ArrayList<>();

    private boolean isArabicLocale;

    @Override
    protected void buildModels() {
        for (Car car : carList) {
            CarViewModel carViewModel = new CarViewModel(car, isArabicLocale);
            new CarViewModel_().id(carViewModel.getCarID()).car(carViewModel)
                    .listener(carViewListener).addTo(this);
        }

        loadingViewModel_.addIf(loading, this);
        errorLoadingViewModel_.listener(errorViewListener).addIf(errorLoading, this);
    }

    public void setArabicLocale(boolean arabicLocale) {
        isArabicLocale = arabicLocale;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
        requestModelBuild();
    }

    public void setErrorLoading(boolean errorLoading) {
        this.errorLoading = errorLoading;
        requestModelBuild();
    }

    public void setCarViewListener(CarView.Listener carViewListener) {
        this.carViewListener = carViewListener;
    }

    public void setErrorViewListener(View.OnClickListener errorViewListener) {
        this.errorViewListener = errorViewListener;
    }

    public void addCars(List<Car> newCars) {
        for (Car car : newCars)
            if (!carList.contains(car))
                carList.add(car);

        requestModelBuild();
    }

    public void setCars(List<Car> cars) {
        this.carList = cars;

        requestModelBuild();
    }

    public void clear() {
        carList.clear();
        requestModelBuild();
    }
}
