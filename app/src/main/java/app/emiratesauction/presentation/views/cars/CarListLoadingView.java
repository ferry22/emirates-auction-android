package app.emiratesauction.presentation.views.cars;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.airbnb.epoxy.ModelView;
import com.facebook.shimmer.ShimmerFrameLayout;

import app.emiratesauction.R;
import app.emiratesauction.presentation.views.BaseView;
import butterknife.BindView;
import butterknife.ButterKnife;

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
public class CarListLoadingView extends BaseView {
    @BindView(R.id.cars_loading_view_container)
    ShimmerFrameLayout itemsLoadingView;

    public CarListLoadingView(Context context) {
        super(context, null);
    }

    public CarListLoadingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        itemsLoadingView.startShimmer();
    }

    @Override
    public int getViewLayout() {
        return R.layout.car_list_loading_view_layout;
    }

    @Override
    public void identifyViews(View view) {
        ButterKnife.bind(this, view);
    }
}
