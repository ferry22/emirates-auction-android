package app.emiratesauction.presentation.views.cars;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.epoxy.AfterPropsSet;
import com.airbnb.epoxy.CallbackProp;
import com.airbnb.epoxy.ModelProp;
import com.airbnb.epoxy.ModelView;

import java.util.concurrent.TimeUnit;

import app.emiratesauction.R;
import app.emiratesauction.presentation.screens.CarViewModel;
import app.emiratesauction.presentation.utils.GlideApp;
import app.emiratesauction.presentation.views.BaseView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
public class CarView extends BaseView {
    @BindView(R.id.car_image)
    ImageView carImage;
    @BindView(R.id.car_name)
    TextView carName;
    @BindView(R.id.car_price)
    TextView carPrice;
    @BindView(R.id.car_currency)
    TextView carCurrency;
    @BindView(R.id.car_bids)
    TextView carBids;
    @BindView(R.id.car_lot)
    TextView carLot;
    @BindView(R.id.car_time_left)
    TextView carTimeLeft;
    @BindView(R.id.btn_favorite)
    ImageView btnFavorite;

    private CarViewModel carViewModel;

    private Listener listener;

    public CarView(Context context) {
        super(context, null);
    }

    public CarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    @ModelProp
    public void setCar(CarViewModel carViewModel) {
        this.carViewModel = carViewModel;
    }

    @CallbackProp
    public void setListener(@Nullable Listener listener) {
        this.listener = listener;
    }

    @AfterPropsSet
    public void draw() {
        GlideApp.with(getContext())
                .load(carViewModel.getImage())
                .centerCrop()
                .error(R.color.colorImagePlaceHolder)
                .into(carImage);

        carName.setText(carViewModel.getName());

        carPrice.setText(carViewModel.getPrice());
        carCurrency.setText(carViewModel.getCurrency());

        carLot.setText(String.valueOf(carViewModel.getLotsNum()));
        carBids.setText(String.valueOf(carViewModel.getBidsNum()));
        carTimeLeft.setText(carViewModel.getTimeLeft());

        // Under 5 mints, color of time label will be change to red till it get updated.
        if (TimeUnit.SECONDS.toMinutes(carViewModel.getDiffInSeconds()) <= 5)
            carTimeLeft.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        else
            carTimeLeft.setTextColor(ContextCompat.getColor(getContext(), R.color.colorTextPrimary));
        
        if (carViewModel.isFavorite())
            btnFavorite.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.baseline_favorite_black_24));
        else
            btnFavorite.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.baseline_favorite_border_black_24));

        btnFavorite.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary));
    }

    @OnClick(R.id.btn_favorite)
    public void onFavoriteClicked() {
        if (listener != null) listener.onFavoriteClicked(carViewModel.getCarID());
    }

    @Override
    public int getViewLayout() {
        return R.layout.car_view_layout;
    }

    @Override
    public void identifyViews(View view) {
        ButterKnife.bind(this, view);
    }

    public interface Listener {
        void onFavoriteClicked(int carId);
    }
}
