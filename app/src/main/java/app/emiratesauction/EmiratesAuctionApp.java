package app.emiratesauction;

import android.app.Application;
import android.content.Context;

import app.emiratesauction.presentation.injection.AppComponent;
import app.emiratesauction.presentation.injection.AppModule;
import app.emiratesauction.presentation.injection.DaggerAppComponent;
import app.emiratesauction.presentation.injection.NetworkModule;
import app.emiratesauction.presentation.utils.LocaleHelper;

public class EmiratesAuctionApp extends Application {
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        mAppComponent = DaggerAppComponent
                .builder()
                .appModule(new AppModule(getApplicationContext()))
                .networkModule(new NetworkModule(BuildConfig.BASE_URL))
                .build();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base));
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }
}
