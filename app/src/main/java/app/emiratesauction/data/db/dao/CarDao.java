package app.emiratesauction.data.db.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import app.emiratesauction.data.models.Car;

@Dao
public interface CarDao {
    @Query("SELECT * FROM car")
    List<Car> getAll();

    @Query("SELECT * FROM car WHERE `carID` LIKE :id LIMIT 1")
    Car findByKey(long id);

    @Insert
    void insertAll(List<Car> cars);

    @Query("DELETE FROM car")
    void deleteAll();
}
