package app.emiratesauction.data.api;

import app.emiratesauction.data.api.models.CarsResponse;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AuctionsApi {
    @GET("carsonline")
    Call<CarsResponse> getCars();
}
