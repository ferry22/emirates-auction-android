package app.emiratesauction.data.api.repository;

import java.util.List;

import app.emiratesauction.data.api.callbacks.BaseCallbackWithList;
import app.emiratesauction.data.models.Car;

public interface CarsDataSource {
    void getCars(BaseCallbackWithList<Car> callback);

    void saveCars(List<Car> cars);
}
