package app.emiratesauction.data.api.repository.local;

import java.util.List;

import app.emiratesauction.data.api.callbacks.BaseCallbackWithList;
import app.emiratesauction.data.api.repository.CarsDataSource;
import app.emiratesauction.data.db.AppDatabase;
import app.emiratesauction.data.models.Car;

public class CarsLocalDataSource implements CarsDataSource {
    private static CarsLocalDataSource INSTANCE = null;

    private AppDatabase appDatabase;

    public static CarsLocalDataSource getInstance(AppDatabase appDatabase) {
        if (INSTANCE == null)
            return new CarsLocalDataSource(appDatabase);

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    private CarsLocalDataSource(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    @Override
    public void getCars(BaseCallbackWithList<Car> callback) {
        List<Car> carList = appDatabase.carDao().getAll();

        if (carList == null || carList.isEmpty()) {
            callback.error();
            return;
        }

        callback.success(carList);
    }

    @Override
    public void saveCars(List<Car> cars) {
        appDatabase.carDao().deleteAll();
        appDatabase.carDao().insertAll(cars);
    }
}
