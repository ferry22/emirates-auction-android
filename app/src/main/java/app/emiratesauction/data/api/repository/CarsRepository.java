package app.emiratesauction.data.api.repository;

import java.util.List;

import app.emiratesauction.data.api.callbacks.BaseCallbackWithList;
import app.emiratesauction.data.models.Car;

public class CarsRepository implements CarsDataSource {
    private static CarsRepository INSTANCE = null;

    private CarsDataSource carsRemoteDataSource;
    private CarsDataSource carsLocalDataSource;

    private CarsRepository(CarsDataSource carsRemoteDataSource, CarsDataSource carsLocalDataSource) {
        this.carsRemoteDataSource = carsRemoteDataSource;
        this.carsLocalDataSource = carsLocalDataSource;
    }

    public static CarsRepository getInstance(CarsDataSource carsRemoteDataSource, CarsDataSource carsLocalDataSource) {
        if (INSTANCE == null)
            return new CarsRepository(carsRemoteDataSource, carsLocalDataSource);

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public void getCars(final BaseCallbackWithList<Car> callback) {
        carsRemoteDataSource.getCars(new BaseCallbackWithList<Car>() {
            @Override
            public void success(List<Car> carList) {
                carsLocalDataSource.saveCars(carList);
                callback.success(carList);
            }

            @Override
            public void error() {
                getItemsFromLocalDataSource(callback);
            }
        });
    }

    private void getItemsFromLocalDataSource(BaseCallbackWithList<Car> callback) {
        carsLocalDataSource.getCars(callback);
    }

    @Override
    public void saveCars(List<Car> cars) {
        // ignored
    }
}
