package app.emiratesauction.data.api.repository.remote;

import java.util.List;

import app.emiratesauction.data.api.AuctionsApi;
import app.emiratesauction.data.api.callbacks.BaseCallbackWithList;
import app.emiratesauction.data.api.models.CarsResponse;
import app.emiratesauction.data.api.repository.CarsDataSource;
import app.emiratesauction.data.models.Car;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarsRemoteDataSource implements CarsDataSource {
    private static CarsRemoteDataSource INSTANCE = null;

    private AuctionsApi auctionsApi;

    private CarsRemoteDataSource(AuctionsApi auctionsApi) {
        this.auctionsApi = auctionsApi;
    }

    public static CarsRemoteDataSource getInstance(AuctionsApi auctionsApi) {
        if (INSTANCE == null)
            return new CarsRemoteDataSource(auctionsApi);

        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public void getCars(final BaseCallbackWithList<Car> callback) {
        auctionsApi.getCars().enqueue(new Callback<CarsResponse>() {
            @Override
            public void onResponse(Call<CarsResponse> call, Response<CarsResponse> response) {
                if (response.isSuccessful()) callback.success(response.body().getCars());
                else callback.error();
            }

            @Override
            public void onFailure(Call<CarsResponse> call, Throwable t) {
                callback.error();
            }
        });
    }

    @Override
    public void saveCars(List<Car> cars) {
        // locally implemented
    }
}
