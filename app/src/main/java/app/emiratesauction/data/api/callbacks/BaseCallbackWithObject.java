package app.emiratesauction.data.api.callbacks;

public interface BaseCallbackWithObject<T> {
    void success(T data);

    void error();
}
