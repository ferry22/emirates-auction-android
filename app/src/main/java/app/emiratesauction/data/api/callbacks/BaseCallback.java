package app.emiratesauction.data.api.callbacks;

public interface BaseCallback {
    void success();

    void error();
}
