# Emirates Auction ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

## Architecture

- The app is build on clean MVP + Clean Architecture design pattern.

So you are expecting to have 3 main layers Data, Domain and Presentation layers.

![MVP + Clean Architecture](https://github.com/googlesamples/android-architecture/wiki/images/mvp-clean.png)

## Notes

- Offline use: the app is caching API responses for offline use.

- For the of demo, I've intercepted the second car and changed the end data to be less than 5 mins so you can this feature can be tested easy.

- Implemented a popup view for sorting cars with endDate, price and year of the car.

- For the of demo, auto refresh interval is 1 minute.

- Implemented favorite button (locally without APIs).

- Git Flow has been used. 

- Product flavors: the app is product-flavor-ready and there should be another url for staging data.

- There is no pagination but the app is ready for it. Recommended to paginate the results.

- I've tried to design layout same as screenshot attached with the assignment but i have made some minor changes according to my point of view.